/**
 * @File Name          : bearTile.js
 * @Description        :
 * @Author             : samantha.reilly@pacificmags.com.au
 * @Group              :
 * @Last Modified By   : samantha.reilly@pacificmags.com.au
 * @Last Modified On   : 04/02/2020, 2:55:34 pm
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    04/02/2020   samantha.reilly@pacificmags.com.au     Initial Version
**/
import { LightningElement, api } from 'lwc';
import ursusResources from '@salesforce/resourceUrl/ursus_park';
export default class BearTile extends LightningElement {
	@api bear;
	appResources = {
		bearSilhouette: ursusResources + '/img/standing-bear-silhouette.png'
	};
	handleOpenRecordClick() {
		const selectEvent = new CustomEvent('bearview', {
			detail: this.bear.Id
		});
		this.dispatchEvent(selectEvent);
	}
}
