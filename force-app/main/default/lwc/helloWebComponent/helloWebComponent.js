/**
 * @File Name          : helloWebComponent.js
 * @Description        :
 * @Author             : samantha.reilly@pacificmags.com.au
 * @Group              :
 * @Last Modified By   : samantha.reilly@pacificmags.com.au
 * @Last Modified On   : 04/02/2020, 1:27:43 pm
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    04/02/2020   samantha.reilly@pacificmags.com.au     Initial Version
**/
import { LightningElement, track } from 'lwc';

export default class HelloWebComponent extends LightningElement {
	@track greeting = 'Trailblazer';

	handleGreetingChange(event) {
		this.greeting = event.target.value;
	}
	get capitalizedGreeting() {
		return `Hello ${this.greeting.toUpperCase()}!`;
	}

	currentDate = new Date().toDateString();
}
