/**
 * @File Name          : bearSupervisor.js
 * @Description        : 
 * @Author             : samantha.reilly@pacificmags.com.au
 * @Group              : 
 * @Last Modified By   : samantha.reilly@pacificmags.com.au
 * @Last Modified On   : 04/02/2020, 1:55:14 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    04/02/2020   samantha.reilly@pacificmags.com.au     Initial Version
**/
import { LightningElement, api, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import SUPERVISOR_FIELD from '@salesforce/schema/Bear__c.Supervisor__c';

const bearFields = [ SUPERVISOR_FIELD ];

export default class BearSupervisor extends LightningElement {
	@api recordId; // Bear Id
	@wire(getRecord, { recordId: '$recordId', fields: bearFields })
	bear;
	get supervisorId() {
		return getFieldValue(this.bear.data, SUPERVISOR_FIELD);
	}
}
